
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  username: string = 'Yeshwanth'; // Dummy username, can be set dynamically

  constructor(private router: Router) {}

  ngOnInit() {
    // Fetch the username dynamically if needed
  }
}
